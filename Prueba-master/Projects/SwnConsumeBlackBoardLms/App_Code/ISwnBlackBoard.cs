﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.IO;

using UinSurgentesEntidades;
using Renci.SshNet;
using Renci.SshNet.Sftp;

[ServiceContract]
public interface ISwnBlackBoard
{
    
    List<Usuario> obtenerUsuario();
    [OperationContract]
     void GenerarArchivoCsv();

    [OperationContract]
    List<string>  ObtenerRutaDirectorio(string astrRuta);

    [OperationContract]
    void SubirArchivo( List<string>astrNombreArchivos);
   
    [OperationContract]
     bool ConectarClienteSftp();

    
}
