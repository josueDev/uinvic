﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;


namespace UinSurgentesEntidades
{
    
    [DataContract]
    [Serializable]
    public class Usuario
    {
        private string username;
        private int idnumber;
        private string email;
        private string auth;
        private string password;
        private string firstname;
        private string lastname;
        private string phone;
        private string city;


        [DataMember]
        public string UserName
        {   get{ return username;}
            set{username = value;}
        }

        [DataMember]
        public int Idnumber {
            get { return idnumber; }
            set { idnumber=value;}
        }
        [DataMember]
        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        [DataMember]
        public string Auth
        {
            get { return auth; }
            set { auth = value; }
        }
        [DataMember]
        public string Password
        {
            get { return password; }
            set { password = value; }
        }
        [DataMember]
        public string Firstname
        {
            get { return firstname; }
            set { firstname = value; }
        }
        [DataMember]
        public string Lastname
        {
            get { return lastname; }
            set { lastname = value; }
        }
        [DataMember]
        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }
        [DataMember]
        public string City
        {
            get { return city; }
            set { city = value; }
        }
    }
}
