﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;


using UinSurgentesEntidades;

[ServiceContract]
public interface ISwnBlackBoard
{
    
    List<Usuario> obtenerUsuario();
    [OperationContract]
     void GenerarArchivoCsv();
}
