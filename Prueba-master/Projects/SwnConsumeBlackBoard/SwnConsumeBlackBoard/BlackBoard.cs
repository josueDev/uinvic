﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

using UinSurgentesEntidades;
using RDNUinSurgentesNegocio;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using System.Data;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Excel;  


[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
public class BlackBoard : ISwnBlackBoard
{

    public List<Usuario> obtenerUsuario()
    {
        List<Usuario> lstUsuario = new List<Usuario>();
        RDNGeneraCsv pobjRdn = new RDNGeneraCsv();
        lstUsuario = pobjRdn.ListarUsuario();

        return lstUsuario;
    }

    public void  GenerarArchivoCsv()
    {

        List<Usuario> lstUsurio = new List<Usuario>();

        lstUsurio = obtenerUsuario();
        var sw = new StreamWriter(@"C:\test.xml");
        var serializar = new XmlSerializer(typeof(List<Usuario>));
        serializar.Serialize(sw, lstUsurio);

        Stream s;
        
        sw.Close();
        
        XmlDocument doc = new XmlDocument();

        doc.Load(@"C:\test.xml");

        System.Data.DataTable dt = CreateDataTableFromXml(doc);

         ExportDataTableToExcel(dt, "xls");
      

    //  return doc.InnerXml;
    }

    /*
      
      XmlTextWriter xmlWriter = new XmlTextWriter(stream, Encoding.UTF8); 
      xmlWriter.Formatting = System.Xml.Formatting.Indented; 
      xmlWriter.WriteStartDocument(); 
      xmlWriter.WriteStartElement("Root"); 
      xmlWriter.WriteEndElement(); 
      xmlWriter.Flush(); 

      XmlDocument doc = new XmlDocument(); 
      stream.Position = 0; 
      doc.Load(stream); 
      doc.Save(Console.Out); 
     }
     */
    public System.Data.DataTable CreateDataTableFromXml(XmlDocument XmlFile)
    {
        System.Data.DataTable Dt = new System.Data.DataTable();
        try
        {
            


            XmlTextReader r = new XmlTextReader(new StringReader(XmlFile.OuterXml));
            DataSet ds = new DataSet();
            ds.ReadXml(r);
            Dt.Load(ds.CreateDataReader());
        }
        catch (Exception ex)
        {

        }
        return Dt;
    }


    private void ExportDataTableToExcel(System.Data.DataTable table, string Xlfile)
    {

        Microsoft.Office.Interop.Excel.Application excel = new Microsoft.Office.Interop.Excel.Application();
        Workbook book = excel.Application.Workbooks.Add(Type.Missing);
        excel.Visible = false;
        excel.DisplayAlerts = false;
        Worksheet excelWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)book.ActiveSheet;
        excelWorkSheet.Name = table.TableName;

        
        for (int i = 1; i < table.Columns.Count + 1; i++) // Creating Header Column In Excel  
        {
            excelWorkSheet.Cells[1, i] = table.Columns[i - 1].ColumnName;
          
        }


        
        for (int j = 0; j < table.Rows.Count; j++) // Exporting Rows in Excel  
        {
            for (int k = 0; k < table.Columns.Count; k++)
            {
                excelWorkSheet.Cells[j + 2, k + 1] = table.Rows[j].ItemArray[k].ToString();
            }

          
        }


        book.SaveAs("CSV");
        book.Close(true);
        excel.Quit();

        Marshal.ReleaseComObject(book);
        Marshal.ReleaseComObject(book);
        Marshal.ReleaseComObject(excel);

    }  

}
