﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Membership.OpenAuth;
using System.IO;
using System.Net;
using System.Text;

namespace pruebaServicio.Account
{
    public partial class Register : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterUser.ContinueDestinationPageUrl = Request.QueryString["ReturnUrl"];
        }

        protected void RegisterUser_CreatedUser(object sender, EventArgs e)
        {
            FormsAuthentication.SetAuthCookie(RegisterUser.UserName, createPersistentCookie: false);

            string continueUrl = RegisterUser.ContinueDestinationPageUrl;
            if (!OpenAuth.IsLocalUrl(continueUrl))
            {
                continueUrl = "~/";
            }
            Response.Redirect(continueUrl);
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        { //https://universidadinsurgentes-sandbox.mrooms.net/blocks/conduit/webservices/rest/course.php

            //https://universidadinsurgentes-sandbox.mrooms.net/blocks/conduit/webservices/rest/Course.php
            string url = "https://universidadinsurgentes-sandbox.mrooms.net/blocks/conduit/webservices/rest/course.php", token = "{55fa8fa9-cf0c-40d2-886f-b3a5c4123aa0}", response = "", value = "CF101"; 
            string postData = "method=get_Course&token=" + token + "value=" + value;
            StreamWriter myWriter = null;
            StringBuilder sb = new StringBuilder();
            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(url);
            objRequest.Method = "POST";
            objRequest.ContentType = "application/x-www-form-urlencoded";
            objRequest.ContentLength = postData.Length;
            if (sb.ToString() != "")
            {
                try
                {
                    myWriter = new StreamWriter(objRequest.GetRequestStream());
                    myWriter.Write(postData);
                }
                catch (Exception ex)
                {
                    response = ex.Message;
                }
                finally
                {
                    myWriter.Close();
                }
            }
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            if(objResponse.ToString()!="")
            {
            }
            using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
            {
                response = sr.ReadToEnd();
                Response.Write(response);
                sr.Close();
                // Close and clean up the StreamReader 
            }
        }
    }
}