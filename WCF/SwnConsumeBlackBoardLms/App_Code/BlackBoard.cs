﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using System.Data;
using System.Xml.Linq;
using System.Web;
using Renci.SshNet;
using Renci.SshNet.Sftp;
using System.Web.Configuration;
using log4net;
using log4net.Config;

[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
public class BlackBoard : ISwnBlackBoard
{
    #region Declaracion de variables
    ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    string host;
    int puerto;
    string Usuario;
    string Contraseña;
    SftpClient cliente = null;
    private string pstrCadenaConexion;
    ConnectionInfo pobjInformacionConeccion = null;
    List<string> lst = new List<string>();
    //RDNGeneraCsv pobjRdn = new RDNGeneraCsv();
    bool pobjBolEstaConectado;
    AbdBlackBoard pObjAbdBlackboard = new AbdBlackBoard();
    private DataTable pdtaDatosAlumno;
    DataSet ds = new DataSet();
    #endregion

    public BlackBoard()
    {
        IniciarAplicacion();
    }

    private void IniciarAplicacion()
    {
        host = WebConfigurationManager.AppSettings["Host"].ToString();
        puerto = Convert.ToInt32(WebConfigurationManager.AppSettings["Puerto"].ToString());
        Usuario = WebConfigurationManager.AppSettings["Usuario"].ToString();
        Contraseña = WebConfigurationManager.AppSettings["Contraseña"].ToString();
        pstrCadenaConexion = WebConfigurationManager.AppSettings["lstrCadenaConexion"].ToString();

        pObjAbdBlackboard.IniciarConexion(pstrCadenaConexion);



        string ldatFecha = Convert.ToDateTime(DateTime.Today.ToShortDateString()).ToString("yyyy/MM/dd").Replace("/", "");
        log4net.GlobalContext.Properties["NombreLog"] = ldatFecha;
        string archivo = AppDomain.CurrentDomain.BaseDirectory.ToString() + "log.dll.log4netJ";
        XmlConfigurator.Configure(new System.IO.FileInfo(archivo));
    }

    private void Credenciales()
    {

        try
        {

            pobjInformacionConeccion = new ConnectionInfo(host, puerto, Usuario,
                               new AuthenticationMethod[]{
									new PasswordAuthenticationMethod(Usuario,Contraseña)
								});
        }
        catch (Exception e)
        {
            throw new Exception(e.Message);
        }
    }


    public void GenerarArchivo(string nombreArchivo, byte[] array)
    {
        log.Info("Metodo ObtenerRutaDirectorio :\t" + "CrearCSV");
        try
        {

            log.Info("Metodo ObtenerRutaDirectorio :\t Nombre del Archivo " + nombreArchivo);

            MemoryStream stream = new MemoryStream(array);

            if (cliente == null || cliente.IsConnected == false)
            {
                ConectarClienteSftp();
                cliente.UploadFile(stream, nombreArchivo, null);

            }
            else
            {
                cliente.UploadFile(stream, nombreArchivo, null);

            }


        }
        catch (Exception ex)
        {
            log.Info("Metodo ObtenerRutaDirectorio :\t  Ocurrio lo siguiente " + ex.Message);
        }
    }



    public string ObtenerDocumentos(StreamReader sb)
    {
        FileInfo f = null;
        if (sb != null)
        {

        }

        return null;

    }
    public string ObtenerRutaDirectorio(string astrRuta)
    {
        try
        {

            log.Info("Metodo ObtenerRutaDirectorio :\t" + astrRuta);

            string[] archivos = System.IO.Directory.GetFiles(astrRuta);

            List<string> list = new List<string>();
            //recorremos todos los archivos y los escribimos en consola
            #region Para la conexion
            FileInfo f = null;
            if (cliente == null || cliente.IsConnected == false)
            {
                ConectarClienteSftp();
                foreach (var a in archivos)
                {
                    log.Info("Metodo ObtenerRutaDirectorio Informacion del Archivo:\t" + a);
                    f = new FileInfo(a);
                    if (f.Extension.Equals(".csv"))
                    {
                        string uploadfile = f.FullName;
                        using (var fileStream = new FileStream(uploadfile, FileMode.Open))
                        {
                            cliente.BufferSize = 4 * 1024;

                            char[] delimiters = { ':', '\\' };
                            string partirNombre = f.FullName;
                            string[] cpartirNombre = partirNombre.Split(delimiters);
                            partirNombre = cpartirNombre[2];

                            cliente.UploadFile(fileStream, partirNombre, null);
                        }
                    }

                }
            }
            #endregion
            return "OK";
        }
        catch (Exception e)
        {
            throw new Exception("Ocurrio lo siguiente :\t" + e);
        }

    }
    public bool ConectarClienteSftp()
    {
        try
        {
            Credenciales();

            cliente = new SftpClient(pobjInformacionConeccion);
            cliente.Connect();
            try
            {
                if (pobjBolEstaConectado = cliente.IsConnected)
                {
                    string workingDirectory = string.Empty;
                    var filePaths = cliente.ListDirectory(cliente.WorkingDirectory).ToList();

                    for (int i = 0; i < filePaths.Count; i++)
                    {
                        if (i == 0)
                        {
                            workingDirectory = filePaths[0].ToString();
                        }
                    }

                    workingDirectory = "/" + workingDirectory.Substring(5, 41);
                    cliente.ChangeDirectory(workingDirectory);

                }
                return pobjBolEstaConectado;
            }
            catch (Exception ex)
            {
                return pobjBolEstaConectado;
            }

        }
        catch (Exception lobjExcepcion)
        {
            return pobjBolEstaConectado;
        }

    }
    private void EjecutarConsultas()
    {




        ds.Tables.Add(pObjAbdBlackboard.ObtenerAlumno());
        ds.Tables.Add(pObjAbdBlackboard.ObtenerCurso());
        ds.Tables.Add(pObjAbdBlackboard.ObtenerGrupo());
        ds.Tables.Add(pObjAbdBlackboard.ObtenerMiembrosDeGrupo());
        ds.Tables.Add(pObjAbdBlackboard.ObtenerRoll());
        ds.Tables.Add(pObjAbdBlackboard.ObtenerEnroll());
    }



    public void GenerarArchivoCsv()
    {
        try
        {
            StringBuilder csvResultado;
            StringBuilder csvCurso;
            StringBuilder MiembrosDeGrupo;
            StringBuilder Grupo;
            StringBuilder csvEnrollments;
            StringBuilder csvRoleAssign;
            EjecutarConsultas();
            if (ds.Tables.Count > 0 && ds != null)
            {
                if (ds.Tables.Contains("Alumnos"))
                {
                    csvResultado = ExportaCsv(ds.Tables["Alumnos"]);
                    CrearCSV(csvResultado);
                }
                if (ds.Tables.Contains("ObtenerCurso"))
                {
                    csvCurso = ExportaCsv(ds.Tables["ObtenerCurso"]);
                    CrearCSVcourse(csvCurso);
                }
                if (ds.Tables.Contains("Grupo"))
                {
                    Grupo = ExportaCsv(ds.Tables["Grupo"]);
                }
                if (ds.Tables.Contains("MiembrosDeGrupo"))
                {
                    MiembrosDeGrupo = ExportaCsv(ds.Tables["MiembrosDeGrupo"]);
                }
                if (ds.Tables.Contains("ObtenerRoll"))
                {
                    csvRoleAssign = ExportaCsv(ds.Tables["ObtenerRoll"]);
                    CrearCSVrole_assign(csvRoleAssign);
                }
                if (ds.Tables.Contains("Enroll"))
                {
                    csvEnrollments = ExportaCsv(ds.Tables["Enroll"]);
                    CrearCSVenroll(csvEnrollments);
                }
            }



            // ConectarClienteSftp();
        }
        catch (Exception e)
        {
            throw new Exception("Ocurrio lo siguiente:\t" + e.Message);

        }
    }

    public StringBuilder ExportaCsv(DataTable dtaResultado)
    {
        StringBuilder sb = new StringBuilder();

        IEnumerable<string> NombreColumnas = dtaResultado.Columns.Cast<DataColumn>().
                                          Select(column => column.ColumnName);
        sb.AppendLine(string.Join(",", NombreColumnas));

        foreach (DataRow row in dtaResultado.Rows)
        {
            IEnumerable<string> fields = row.ItemArray.Select(field => field.ToString());
            sb.AppendLine(string.Join(",", fields));
        }

        return sb;
    }

    private void CrearCSV(StringBuilder concat)
    {
        log.Info("Metodo ObtenerRutaDirectorio :\t" + "CrearCSV");

        log.Info("Metodo ObtenerRutaDirectorio :\t" + concat);

        int i = concat.Length;

        log.Info("Metodo ObtenerRutaDirectorio :\t" + i);

        StreamWriter pobjWriter = new StreamWriter("C:\\" + "auth.csv", false);

        pobjWriter.Write(concat);
        pobjWriter.Close();
    }

    private void CrearCSVcourse(StringBuilder concat)
    {

        StreamWriter pobjStream = new StreamWriter("C:\\" + "course.csv", false);

        pobjStream.Write(concat);
        pobjStream.Close();
    }

    private void CrearCSVenroll(StringBuilder concat)
    {

        StreamWriter pobjStream = new StreamWriter("C:\\" + "enroll.csv", false);

        pobjStream.Write(concat);
        pobjStream.Close();
    }

    private void CrearCSVrole_assign(StringBuilder concat)
    {
      




        StreamWriter pobjStream = new StreamWriter("C:\\" + "role_assign.csv", false);


        pobjStream.Write(concat);
        pobjStream.Close();
    }

}

