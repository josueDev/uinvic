﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using UinSurgentesEntidades;
using RDNUinSurgentesNegocio;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using System.Data;
using System.Xml.Linq;
using System.Web;
using Renci.SshNet;
using Renci.SshNet.Sftp;
using System.Web.Configuration;
using log4net;
using log4net.Config;

[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
public class BlackBoard : ISwnBlackBoard
{
    #region Declaracion de variables
    ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    string host;
    int puerto;
    string Usuario;
    string Contraseña;
    SftpClient cliente = null;
    ConnectionInfo pobjInformacionConeccion = null;
    List<string> lst = new List<string>();
    RDNGeneraCsv pobjRdn = new RDNGeneraCsv();
    bool pobjBolEstaConectado;
    #endregion

    public BlackBoard()
    {
        IniciarAplicacion();
    }

    private void IniciarAplicacion()
    {
        host = WebConfigurationManager.AppSettings["Host"].ToString();
        puerto = Convert.ToInt32(WebConfigurationManager.AppSettings["Puerto"].ToString());
        Usuario = WebConfigurationManager.AppSettings["Usuario"].ToString();
        Contraseña = WebConfigurationManager.AppSettings["Contraseña"].ToString();
        string ldatFecha = Convert.ToDateTime(DateTime.Today.ToShortDateString()).ToString("yyyy/MM/dd").Replace("/", "");
        log4net.GlobalContext.Properties["NombreLog"] = ldatFecha;
        string archivo = AppDomain.CurrentDomain.BaseDirectory.ToString() + "log.dll.log4netJ";
        XmlConfigurator.Configure(new System.IO.FileInfo(archivo));
    }

    private void Credenciales()
    {

        try
        {

            pobjInformacionConeccion = new ConnectionInfo(host, puerto, Usuario,
                               new AuthenticationMethod[]{
									new PasswordAuthenticationMethod(Usuario,Contraseña)
								});
        }
        catch (Exception e)
        {
            throw new Exception(e.Message);
        }
    }

    public void GenerarArchivoCsv()
    {
        try
        {
            List<Usuario> lstUsurio = new List<Usuario>();
            List<Course> lstCourse = new List<Course>();
            List<Enrollments> lstEnrollments = new List<Enrollments>();
            List<RoleAssign> lstRoleAssign = new List<RoleAssign>();



            lstUsurio = obtenerUsuario();
            lstCourse = obtenerCurso();
            lstEnrollments = obtenerEnrollments();
            lstRoleAssign = obtenerRoleAssign();
    
            DataTable dtaResultado = ConvertListaaTabla(lstUsurio);
            DataTable dtaCurso = ConvertListaaTabla(lstCourse);
            DataTable dtalstEnrollments = ConvertListaaTabla(lstEnrollments);
            DataTable dtalstRoleAssign = ConvertListaaTabla(lstRoleAssign);

            StringBuilder csvResultado = ExportaCsv(dtaResultado);
            StringBuilder csvCurso = ExportaCsv(dtaCurso);
            StringBuilder csvEnrollments = ExportaCsv(dtalstEnrollments);
            StringBuilder csvRoleAssign = ExportaCsv(dtalstRoleAssign);

            CrearCSV(csvResultado);
            CrearCSVcourse(csvCurso);
            CrearCSVenroll(csvEnrollments);
            CrearCSVrole_assign(csvRoleAssign);
            // ConectarClienteSftp();
        }
        catch (Exception e)
        {
            throw new Exception("Ocurrio lo siguiente:\t" + e.Message);

        }
    }

    public void GenerarArchivo(string nombreArchivo, byte[] array)
    {
        log.Info("Metodo ObtenerRutaDirectorio :\t" + "CrearCSV");
        try
        {

            log.Info("Metodo ObtenerRutaDirectorio :\t Nombre del Archivo " + nombreArchivo);

            MemoryStream stream = new MemoryStream(array);

            if (cliente == null || cliente.IsConnected == false)
            {
                ConectarClienteSftp();
                cliente.UploadFile(stream, nombreArchivo, null);

            }
            else
            {
                cliente.UploadFile(stream, nombreArchivo, null);

            }


        }
        catch (Exception ex)
        {
            log.Info("Metodo ObtenerRutaDirectorio :\t  Ocurrio lo siguiente " + ex.Message);
        }
    }



    public string ObtenerDocumentos(StreamReader sb)
    {
        FileInfo f = null;
        if (sb != null)
        {

        }

        return null;

    }
    public string ObtenerRutaDirectorio(string astrRuta)
    {
        try
        {

            log.Info("Metodo ObtenerRutaDirectorio :\t" + astrRuta);

            string[] archivos = System.IO.Directory.GetFiles(astrRuta);

            List<string> list = new List<string>();
            //recorremos todos los archivos y los escribimos en consola
            #region Para la conexion
            FileInfo f = null;
            if (cliente == null || cliente.IsConnected == false)
            {
                ConectarClienteSftp();
                foreach (var a in archivos)
                {
                    log.Info("Metodo ObtenerRutaDirectorio Informacion del Archivo:\t" + a);
                    f = new FileInfo(a);
                    if (f.Extension.Equals(".csv"))
                    {
                        string uploadfile = f.FullName;
                        using (var fileStream = new FileStream(uploadfile, FileMode.Open))
                        {
                            cliente.BufferSize = 4 * 1024;

                            char[] delimiters = { ':', '\\' };
                            string partirNombre = f.FullName;
                            string[] cpartirNombre = partirNombre.Split(delimiters);
                            partirNombre = cpartirNombre[2];

                            cliente.UploadFile(fileStream, partirNombre, null);
                        }
                    }

                }
            }
            #endregion
            return "OK";
        }
        catch (Exception e)
        {
            throw new Exception("Ocurrio lo siguiente :\t" + e);
        }

    }
    public bool ConectarClienteSftp()
    {
        try
        {
            Credenciales();

            cliente = new SftpClient(pobjInformacionConeccion);
            cliente.Connect();
            try
            {
                if (pobjBolEstaConectado = cliente.IsConnected)
                {
                    string workingDirectory = string.Empty;
                    var filePaths = cliente.ListDirectory(cliente.WorkingDirectory).ToList();

                    for (int i = 0; i < filePaths.Count; i++)
                    {
                        if (i == 0)
                        {
                            workingDirectory = filePaths[0].ToString();
                        }
                    }

                    workingDirectory = "/" + workingDirectory.Substring(5, 41);
                    cliente.ChangeDirectory(workingDirectory);

                }
                return pobjBolEstaConectado;
            }
            catch (Exception ex)
            {
                return pobjBolEstaConectado;
            }

        }
        catch (Exception lobjExcepcion)
        {
            return pobjBolEstaConectado;
        }

    }



    public List<Usuario> obtenerUsuario()
    {
        List<Usuario> lstUsuario = new List<Usuario>();
        pobjRdn = new RDNGeneraCsv();

        lstUsuario = pobjRdn.ListarUsuario();

        return lstUsuario;
    }

    public List<Course> obtenerCurso()
    {
        List<Course> lstCourse = new List<Course>();
        pobjRdn = new RDNGeneraCsv();
        lstCourse = pobjRdn.ListaCurso();

        return lstCourse;
    }

    public List<Enrollments> obtenerEnrollments()
    {
        List<Enrollments> lstEnrollments = new List<Enrollments>();
        pobjRdn = new RDNGeneraCsv();
        lstEnrollments = pobjRdn.ListaEnrollments();

        return lstEnrollments;


    }


    public List<RoleAssign> obtenerRoleAssign()
    {
        List<RoleAssign> lstRoleAssign = new List<RoleAssign>();
        pobjRdn = new RDNGeneraCsv();
        lstRoleAssign = pobjRdn.ListarAsignacionROles();
        return lstRoleAssign;
    }



    public DataTable ConvertListaaTabla(List<string> lstUsurio)
    {

        List<string> p;
        string[] cpartirNombre = null;
        DataTable dtaConvertir = new DataTable();
        DataRow workRow;

        log.Info("Metodo Convertir Lista");
        string posicion = string.Empty;

        for (int pobIntAuxiliar = 0; pobIntAuxiliar <= lstUsurio.Count; pobIntAuxiliar++)
        {
            log.Info("Entrando al For  valor del iterador: " + pobIntAuxiliar);


            if (pobIntAuxiliar == 0)
            {
                dtaConvertir.TableName = lstUsurio[pobIntAuxiliar];
                lstUsurio.RemoveAt(pobIntAuxiliar);
            }

            if (pobIntAuxiliar == 1)
            {
                posicion = lstUsurio[pobIntAuxiliar - 1];

                char[] delimiters = { ',' };
                string partirNombre = posicion;
                cpartirNombre = partirNombre.Split(delimiters);
                for (int i = 0; i < cpartirNombre.Length; i++)
                {
                    log.Info("Nombres de las columnas   " + cpartirNombre[i]);
                    dtaConvertir.Columns.Add(cpartirNombre[i]);
                }
            }


            if (pobIntAuxiliar >= 2)
            {

                posicion = lstUsurio[pobIntAuxiliar - 1];
                char[] delimiters = { ',' };
                string partirNombre = posicion;
                cpartirNombre = partirNombre.Split(delimiters);


                foreach (string s in cpartirNombre)
                {
                    dtaConvertir.Rows.Add(s);
                }

            }
        }

        return null;
    }




    public DataTable ConvertListaaTabla(List<Usuario> lstUsurio)
    {
        DataTable dtaConvertir = new DataTable();
        dtaConvertir.Columns.Add("Idnumber", typeof(int));
        dtaConvertir.Columns.Add("UserName", typeof(string));
        dtaConvertir.Columns.Add("Auth", typeof(string));
        dtaConvertir.Columns.Add("Password", typeof(string));
        dtaConvertir.Columns.Add("Firstname", typeof(string));
        dtaConvertir.Columns.Add("Lastname", typeof(string));
        dtaConvertir.Columns.Add("Phone", typeof(string));
        dtaConvertir.Columns.Add("City", typeof(string));
        dtaConvertir.Columns.Add("Email", typeof(string));

        foreach (Usuario a in lstUsurio)
        {
            DataRow row = dtaConvertir.NewRow();

            row["Idnumber"] = a.Idnumber;
            row["UserName"] = a.UserName;
            row["Auth"] = a.Auth;
            row["Password"] = a.Password;
            row["Firstname"] = a.Firstname;
            row["Lastname"] = a.Lastname;
            row["Phone"] = a.Phone;
            row["City"] = a.City;
            row["Email"] = a.Email;

            dtaConvertir.Rows.Add(row);

        }


        return dtaConvertir;
    }


    public DataTable ConvertListaaTabla(List<Course> lstCurso)
    {
        DataTable dtaConvertir = new DataTable();
        dtaConvertir.Columns.Add("Idnumber", typeof(int));
        dtaConvertir.Columns.Add("ShortName", typeof(string));
        dtaConvertir.Columns.Add("FullName", typeof(string));
        dtaConvertir.Columns.Add("Category", typeof(string));
        dtaConvertir.Columns.Add("Visible", typeof(string));
        dtaConvertir.Columns.Add("CourseTemplate", typeof(string));


        foreach (Course a in lstCurso)
        {
            DataRow row = dtaConvertir.NewRow();

            row["Idnumber"] = a.Idnumber;
            row["ShortName"] = a.ShortName;
            row["FullName"] = a.FullName;
            row["Category"] = a.Category;
            row["Visible"] = a.Visible;
            row["CourseTemplate"] = a.CourseTemplate;


            dtaConvertir.Rows.Add(row);

        }


        return dtaConvertir;
    }

    public DataTable ConvertListaaTabla(List<Enrollments> lstUsurio)
    {
        DataTable dtaConvertir = new DataTable();
        dtaConvertir.Columns.Add("Course", typeof(string));
        dtaConvertir.Columns.Add("User", typeof(string));
        dtaConvertir.Columns.Add("Role", typeof(string));
        dtaConvertir.Columns.Add("Estatus", typeof(string));


        foreach (Enrollments a in lstUsurio)
        {
            DataRow row = dtaConvertir.NewRow();

            row["Course"] = a.Course;
            row["User"] = a.User;
            row["Role"] = a.Role;
            row["Estatus"] = a.Estatus;

            dtaConvertir.Rows.Add(row);

        }


        return dtaConvertir;
    }

    public DataTable ConvertListaaTabla(List<RoleAssign> lstUsurio)
    {
        DataTable dtaConvertir = new DataTable();
        dtaConvertir.Columns.Add("Contexto", typeof(string));
        dtaConvertir.Columns.Add("UserNameStudent", typeof(string));
        dtaConvertir.Columns.Add("UserNameParent", typeof(string));
        dtaConvertir.Columns.Add("Role", typeof(string));


        foreach (RoleAssign a in lstUsurio)
        {
            DataRow row = dtaConvertir.NewRow();

            row["Contexto"] = a.Contexto;
            row["UserNameStudent"] = a.UserNameStudent;
            row["UserNameParent"] = a.UserNameParent;
            row["Role"] = a.Role;
            dtaConvertir.Rows.Add(row);

        }


        return dtaConvertir;
    }


    public StringBuilder ExportaCsv(DataTable dtaResultado)
    {
        StringBuilder sb = new StringBuilder();

        IEnumerable<string> NombreColumnas = dtaResultado.Columns.Cast<DataColumn>().
                                          Select(column => column.ColumnName);
        sb.AppendLine(string.Join(",", NombreColumnas));

        foreach (DataRow row in dtaResultado.Rows)
        {
            IEnumerable<string> fields = row.ItemArray.Select(field => field.ToString());
            sb.AppendLine(string.Join(",", fields));
        }

        return sb;
    }

    private void CrearCSV(StringBuilder concat)
    {
        log.Info("Metodo ObtenerRutaDirectorio :\t" + "CrearCSV");

        log.Info("Metodo ObtenerRutaDirectorio :\t" + concat);

        int i = concat.Length;

        log.Info("Metodo ObtenerRutaDirectorio :\t" + i);

        StreamWriter pobjWriter = new StreamWriter("C:\\" + "auth.csv", false);

        pobjWriter.Write(concat);
        pobjWriter.Close();
    }

    private void CrearCSVcourse(StringBuilder concat)
    {

        StreamWriter pobjStream = new StreamWriter("C:\\" + "course.csv", false);

        pobjStream.Write(concat);
        pobjStream.Close();
    }

    private void CrearCSVenroll(StringBuilder concat)
    {

        StreamWriter pobjStream = new StreamWriter("C:\\" + "enroll.csv", false);

        pobjStream.Write(concat);
        pobjStream.Close();
    }

    private void CrearCSVrole_assign(StringBuilder concat)
    {
        StreamReader d = new StreamReader("");




        StreamWriter pobjStream = new StreamWriter("C:\\" + "role_assign.csv", false);


        pobjStream.Write(concat);
        pobjStream.Close();
    }













}

